# download statements

    'data.frame':	0 obs. of  32 variables:
     $ accountsPayable        : num 
     $ capitalSurplus         : logi 
     $ commonStock            : num 
     $ currency               : chr 
     $ currentAssets          : num 
     $ currentCash            : num 
     $ currentLongTermDebt    : num 
     $ filingType             : chr 
     $ goodwill               : int 
     $ intangibleAssets       : int 
     $ inventory              : num 
     $ longTermDebt           : num 
     $ longTermInvestments    : num 
     $ minorityInterest       : int 
     $ netTangibleAssets      : num 
     $ otherAssets            : num 
     $ otherCurrentAssets     : num 
     $ otherCurrentLiabilities: num 
     $ otherLiabilities       : num 
     $ propertyPlantEquipment : num 
     $ receivables            : num 
     $ reportDate             : 'Date' num(0) 
     $ retainedEarnings       : num 
     $ shareholderEquity      : num 
     $ shortTermInvestments   : num 
     $ symbol                 : chr 
     $ totalAssets            : num 
     $ totalCurrentLiabilities: num 
     $ totalLiabilities       : num 
     $ treasuryStock          : int 
     $ type                   : chr 
     $ period                 : chr 

